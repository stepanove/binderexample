import QtQuick 2.0
import QtQuick.Controls 2.4
import Binders 1.0
Column
{    
    property Binder binder
    //You might use QtObject instead of Binder here, it will work as well, but you will not have code completion
    //property QtObject binder
    property string title: ""
    spacing: 10

    Label
    {
        text: title
        font.bold: true
    }
    Label
    {
        text: "Int value: "+binder.intValue
    }

    SpinBox
    {
        value: binder.intValue
        onValueChanged: binder.intValue=value
    }

    Label
    {
        text: "Real value: "+binder.realValue
    }

    TextField
    {
        text: binder.realValue
        onAccepted: binder.realValue=text
        validator: DoubleValidator{locale: "С"}
    }

    Label
    {
        text: "Bool value: "+binder.boolValue
    }

    Switch
    {
        checked: binder.boolValue
        onCheckedChanged: binder.boolValue=checked
    }
}
