import QtQuick 2.15
import QtQuick.Window 2.15
import Binders 1.0
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    Binder
    {
        id: someBinder
    }

    Row
    {
        spacing: 10
        BinderView
        {
            title: "someBinder"
            binder: someBinder
        }
        BinderView
        {
            title: "another view of someBinder"
            binder: someBinder
        }
        BinderView
        {
            title: "otherBinder"
            binder: otherBinder
        }
        BinderView
        {
            title: "yetAnotherBinder"
            binder: Binder{id: yetAnotherBinder}
        }
    }
}
