#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "binder.h"
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    //Make Binder class known to QML engine, so Binder instances can be created from QML and used just like any other QML item
    qmlRegisterType<Binder>("Binders",1,0,"Binder");


    QQmlApplicationEngine engine;

    //Create object on C++ side and set is a contect property, it will be known to the QML engine as "otherBinder"
    Binder binder;
    engine.rootContext()->setContextProperty("otherBinder",&binder);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
