#ifndef BINDER_H
#define BINDER_H

#include <QObject>

class Binder : public QObject
{
    Q_OBJECT

    //All these setters, getters and signals can be automatically generated with right click on Q_PROPERTY->Refactor->Generate missing Q_PROPERTY Members
    //All the params declared with Q_PROPERTY will be available on QML side just like any Item's properties
    Q_PROPERTY(qreal realValue READ realValue WRITE setRealValue NOTIFY realValueChanged)
    Q_PROPERTY(bool boolValue READ boolValue WRITE setBoolValue NOTIFY boolValueChanged)
    Q_PROPERTY(int intValue READ intValue WRITE setIntValue NOTIFY intValueChanged)
    qreal m_realValue{0};

    bool m_boolValue{false};

    int m_intValue{0};

public:
    explicit Binder(QObject *parent = nullptr);

    qreal realValue() const
    {
        return m_realValue;
    }

    bool boolValue() const
    {
        return m_boolValue;
    }

    int intValue() const
    {
        return m_intValue;
    }

public slots:
    void setRealValue(qreal realValue)
    {
        qWarning("Floating point comparison needs context sanity check");
        if (qFuzzyCompare(m_realValue, realValue))
            return;

        m_realValue = realValue;
        emit realValueChanged(m_realValue);
    }

    void setBoolValue(bool boolValue)
    {
        if (m_boolValue == boolValue)
            return;

        m_boolValue = boolValue;
        emit boolValueChanged(m_boolValue);
    }

    void setIntValue(int intValue)
    {
        if (m_intValue == intValue)
            return;

        m_intValue = intValue;
        emit intValueChanged(m_intValue);
    }

signals:

    void realValueChanged(qreal realValue);
    void boolValueChanged(bool boolValue);
    void intValueChanged(int intValue);
};

#endif // BINDER_H
